namespace Calendar.Domain.Interfaces;

public interface IDomainEventDispatcher
{
  Task DispatchAndClearEvents(IEnumerable<BaseEntity> entitiesWithEvents);
}