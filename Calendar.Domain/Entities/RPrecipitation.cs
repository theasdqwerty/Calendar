namespace Calendar.Domain.Entities;

public class RPrecipitation : BaseEntity
{
    public string? Precipitation { get; set; }

    public virtual ICollection<WeatherCalendar> WeatherCalendars { get; set; } = new List<WeatherCalendar>();
}
