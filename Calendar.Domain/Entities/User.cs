using System.ComponentModel.DataAnnotations;

namespace Calendar.Domain.Entities;

/// <summary>
/// Base user entity.
/// </summary>
public class User : BaseEntity
{
    public string? FirstName { get; set; }

    public string? SecondName { get; set; }

    public string? LastName { get; set; }

    public int? RegionCodeId { get; set; }

    public string? Email { get; set; }

    public string? PhoneNumber { get; set; }

    public virtual Region? RegionCode { get; set; }

    public virtual ICollection<Stead> Steads { get; set; } = new List<Stead>();
}
