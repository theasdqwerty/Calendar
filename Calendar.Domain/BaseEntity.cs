using System.ComponentModel.DataAnnotations.Schema;

namespace Calendar.Domain;

/// <summary>
/// Base root entity.
/// </summary>
public class BaseEntity
{
    /// <summary>
    /// Primary key.
    /// </summary>
    public int Id { get; set; }
    
    private List<DomainEventBase> _domainEvents = new ();
    
    [NotMapped]
    public IEnumerable<DomainEventBase> DomainEvents => _domainEvents.AsReadOnly();

    protected void RegisterDomainEvent(DomainEventBase domainEvent) => _domainEvents.Add(domainEvent);
    protected void ClearDomainEvents() => _domainEvents.Clear();

}