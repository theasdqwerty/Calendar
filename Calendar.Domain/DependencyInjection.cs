using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace Calendar.Domain;

/// <summary>
/// Dependency injection for the domain layer.
/// </summary>
public static class DependencyInjection
{
    /// <summary>
    /// Extension method for configure.
    /// </summary>
    /// <param name="services">Serivce collection</param>
    /// <returns>Service collection.</returns>
    public static IServiceCollection AddDomain(this IServiceCollection services)
    {
        var assembly = typeof(DependencyInjection).Assembly;
        services.AddMediatR(configuration => configuration.RegisterServicesFromAssembly(assembly));
        services.AddValidatorsFromAssembly(assembly);
        return services;
    }
}