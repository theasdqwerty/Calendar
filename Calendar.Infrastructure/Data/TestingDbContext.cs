using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Infrastructure.Data;

public class TestingDbContext : DbContext , ITestingDbContext
{
    public DbSet<Customer> Customers => Set<Customer>();
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Filename=testing.sqlite");
        base.OnConfiguring(optionsBuilder);
    }
}

public interface ITestingDbContext
{
    DbSet<Customer> Customers { get; }
}

[PrimaryKey("Id")]
public class Customer
{
    public int Id { get; }
    public string Name { get; set; } 
}