using System.Reflection;
using Calendar.Application.Interfaces;
using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Infrastructure.Data;

public class ApplicationDbContext : DbContext, IApplicationDbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=192.168.1.104;Port=5432;Database=garden_calendary;Username=garden_calendary;Password=summer");
    
    public DbSet<Plant> Plant => Set<Plant>();
    public DbSet<RPrecipitation> RPrecipitation => Set<RPrecipitation>();
    public DbSet<RReestrObject> RReestrObject => Set<RReestrObject>();
    public DbSet<Region> Region => Set<Region>();
    public DbSet<Stead> Stead => Set<Stead>();
    public DbSet<User> User => Set<User>();
    public DbSet<WeatherCalendar> WeatherCalendar => Set<WeatherCalendar>();
}
