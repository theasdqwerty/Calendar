using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class SteadConfiguration : IEntityTypeConfiguration<Stead>
{
    public void Configure(EntityTypeBuilder<Stead> builder)
    {
        builder.HasKey(e => e.Id).HasName("stead_pkey");
        
        builder.ToTable("stead");
        
        builder.Property(e => e.Id).HasColumnName("id");
        builder.Property(e => e.ObjectId).HasColumnName("object_id");
        builder.Property(e => e.PlantId).HasColumnName("plant_id");
        builder.Property(e => e.RegionCodeId).HasColumnName("region_code_id");
        builder.Property(e => e.StartDate).HasColumnName("start_date");
        builder.Property(e => e.UserId).HasColumnName("user_id");
        
        builder.HasOne(d => d.Object).WithMany(p => p.Steads)
            .HasForeignKey(d => d.ObjectId)
            .HasConstraintName("stead_object_id_fkey");
        
        builder.HasOne(d => d.Plant).WithMany(p => p.Steads)
            .HasForeignKey(d => d.PlantId)
            .HasConstraintName("stead_plant_id_fkey");
        
        builder.HasOne(d => d.RegionCode).WithMany(p => p.Steads)
            .HasForeignKey(d => d.RegionCodeId)
            .HasConstraintName("stead_region_code_fkey");                   
        
        builder.HasOne(d => d.User).WithMany(p => p.Steads)
            .HasForeignKey(d => d.UserId)
            .HasConstraintName("stead_user_id_fkey");
    }
}