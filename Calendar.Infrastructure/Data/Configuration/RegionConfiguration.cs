using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class RegionConfiguration : IEntityTypeConfiguration<Region>
{
    public void Configure(EntityTypeBuilder<Region> builder)
    {
        builder.HasKey(e => e.RegionCodeId).HasName("region_pkey");
        builder.ToTable("region");
        builder.Property(e => e.RegionCodeId)
            .ValueGeneratedNever()
            .HasColumnName("region_code_id");
        
        builder.Property(e => e.RegionName)
            .HasColumnType("character varying")
            .HasColumnName("region_name");
    }
}