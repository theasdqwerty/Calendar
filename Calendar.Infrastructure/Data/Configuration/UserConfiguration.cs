using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasKey(e => e.Id).HasName("users_pkey");
        
        builder.ToTable("users");

        builder.Property(e => e.Id).HasColumnName("id");
        builder.Property(e => e.Email)
            .HasColumnType("character varying")
            .HasColumnName("email");
        builder.Property(e => e.FirstName)
            .HasColumnType("character varying")
            .HasColumnName("first_name");
        builder.Property(e => e.LastName)
            .HasColumnType("character varying")
            .HasColumnName("last_name");
        builder.Property(e => e.PhoneNumber)
            .HasColumnType("character varying")
            .HasColumnName("phone_number");
        builder.Property(e => e.RegionCodeId).HasColumnName("region_code_id");
        builder.Property(e => e.SecondName)
            .HasColumnType("character varying")
            .HasColumnName("second_name");
        builder.HasOne(d => d.RegionCode).WithMany(p => p.Users)
            .HasForeignKey(d => d.RegionCodeId)
            .HasConstraintName("users_region_code_fkey");                   
    }
}