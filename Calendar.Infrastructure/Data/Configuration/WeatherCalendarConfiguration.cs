using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class WeatherCalendarConfiguration : IEntityTypeConfiguration<WeatherCalendar>
{
    public void Configure(EntityTypeBuilder<WeatherCalendar> builder)
    {
        builder.HasKey(e => e.Id).HasName("weather_calendar_pkey");
     
        builder.ToTable("weather_calendar");
     
        builder.Property(e => e.Id).HasColumnName("id");
        builder.Property(e => e.DateTame).HasColumnName("date_tame");
        builder.Property(e => e.ObjectId).HasColumnName("object_id");
        builder.Property(e => e.PrecipitationId).HasColumnName("precipitation_id");
        builder.Property(e => e.RegionCodeId).HasColumnName("region_code_id");
        builder.Property(e => e.TemperaturaMax).HasColumnName("temperatura_max");
        builder.Property(e => e.TemperaturaMin).HasColumnName("temperatura_min");
     
        builder.HasOne(d => d.Object).WithMany(p => p.WeatherCalendars)
            .HasForeignKey(d => d.ObjectId)
            .HasConstraintName("weather_calendar_object_id_fkey");
     
        builder.HasOne(d => d.Precipitation).WithMany(p => p.WeatherCalendars)
            .HasForeignKey(d => d.PrecipitationId)
            .HasConstraintName("weather_calendar_precipitation_id_fkey");
     
        builder.HasOne(d => d.RegionCode).WithMany(p => p.WeatherCalendars)
            .HasForeignKey(d => d.RegionCodeId)
            .HasConstraintName("weather_calendar_region_code_fkey");                 
    }
}