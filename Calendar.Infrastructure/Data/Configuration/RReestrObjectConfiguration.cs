using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class RReestrObjectConfiguration : IEntityTypeConfiguration<RReestrObject>
{
    public void Configure(EntityTypeBuilder<RReestrObject> builder)
    {
        builder.HasKey(e => e.Id).HasName("r$reestr_object_pkey");
     
        builder.ToTable("r$reestr_object");
     
        builder.Property(e => e.Id).HasColumnName("id");
        builder.Property(e => e.AccuweatherId).HasColumnName("accuweather_id");
        builder.Property(e => e.ObjectName)
            .HasColumnType("character varying")
            .HasColumnName("object_name");
        builder.Property(e => e.RegionCodeId).HasColumnName("region_code_id");  
    }
}