using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class PlantConfiguration : IEntityTypeConfiguration<Plant>
{
    public void Configure(EntityTypeBuilder<Plant> builder)
    {
        builder.HasKey(e => e.Id).HasName("plant_pkey");
        builder.ToTable("plant");
        builder.Property(e => e.Id).HasColumnName("id");
        builder.Property(e => e.PlantName)
            .HasColumnType("character varying")
            .HasColumnName("plant_name");
        builder.Property(e => e.SoilMoistureMax).HasColumnName("soil_moisture_max");
        builder.Property(e => e.SoilMoistureMin).HasColumnName("soil_moisture_min");
        builder.Property(e => e.TempMax).HasColumnName("temp_max");
        builder.Property(e => e.TempMin).HasColumnName("temp_min");
    }
}