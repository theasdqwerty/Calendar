using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.Data.Configuration;

public class RPrecipitationConfiguration : IEntityTypeConfiguration<RPrecipitation>
{
    public void Configure(EntityTypeBuilder<RPrecipitation> builder)
    {
        builder.HasKey(e => e.Id).HasName("precipitation_pkey");

        builder.ToTable("r$precipitation");

        builder.Property(e => e.Id)
            .ValueGeneratedNever()
            .HasColumnName("id");
        builder.Property(e => e.Precipitation)
            .HasColumnType("character varying")
            .HasColumnName("precipitation");
    }
}