using Calendar.Application.Enums;
using Calendar.Application.Interfaces;
using Calendar.Application.UserAggregate.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Application.Services;

public class DeleteUserService : IDeleteUserService
{
    private readonly IApplicationDbContext _dbContext;
    private readonly IMediator _mediator;
    
    public DeleteUserService(IMediator mediator, IApplicationDbContext dbContext)
    {
        _mediator = mediator;
        _dbContext = dbContext;
    }
    
    public async Task<Result> DeleteUser(int userId, CancellationToken token)
    {
        var list = await _dbContext.User.ToListAsync(token);
        
        var userToDelete = await _dbContext.User.FirstOrDefaultAsync(e => e.Id == userId, cancellationToken: token);
        if (userToDelete is null)
            return Result.NotFound;

        _dbContext.User.Remove(userToDelete);
        await _dbContext.SaveChangesAsync(token);
        await _mediator.Publish(new DeleteUserEvent(userId), token);
        
        return Result.Success;
    }

}