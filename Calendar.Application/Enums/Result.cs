namespace Calendar.Application.Enums;

public enum Result
{
    Success,
    NotSuccess,
    Error,
    NotFound
}