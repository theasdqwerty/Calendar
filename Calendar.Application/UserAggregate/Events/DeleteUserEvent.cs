using Calendar.Domain;

namespace Calendar.Application.UserAggregate.Events;

public class DeleteUserEvent : DomainEventBase
{
    public int UserId { get; }

    public DeleteUserEvent(int userId) 
        => UserId = userId;
}