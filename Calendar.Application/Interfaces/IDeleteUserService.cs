using Calendar.Application.Enums;

namespace Calendar.Application.Interfaces;

public interface IDeleteUserService
{
    public Task<Result> DeleteUser(int userId, CancellationToken token = default);
}