using Calendar.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Application.Interfaces;

public interface IApplicationDbContext
{
    DbSet<Plant> Plant { get; }

    DbSet<RPrecipitation> RPrecipitation { get;  }

    DbSet<RReestrObject> RReestrObject { get;  }

    DbSet<Region> Region { get;  }

    DbSet<Stead> Stead { get;  }

    DbSet<User> User { get;  }

    DbSet<WeatherCalendar> WeatherCalendar { get;  }

    Task<int> SaveChangesAsync(CancellationToken token);
}