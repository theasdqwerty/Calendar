using Calendar.Application.Interfaces;
using Calendar.Application.UserAggregate.Events;
using MediatR;

namespace Calendar.Application.Handlers;

public class DeleteUserHandler : INotificationHandler<DeleteUserEvent>
{
    private readonly IApplicationDbContext _dbContext;
    public DeleteUserHandler(IApplicationDbContext dbContext) 
        => _dbContext = dbContext;

    public Task Handle(DeleteUserEvent notification, CancellationToken cancellationToken)
    {
        // 
        return Task.CompletedTask;
    }
}