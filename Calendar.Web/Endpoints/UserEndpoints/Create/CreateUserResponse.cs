namespace Calendar.Web.Endpoints.UserEndpoints.Create;

public class CreateUserResponse
{
    public int Id { get; set; }
    public string? FirstName { get; set; }

    public string? SecondName { get; set; }

    public string? LastName { get; set; }

    public int? RegionCodeId { get; set; }

    public string? Email { get; set; }

    public string? PhoneNumber { get; set; }
}