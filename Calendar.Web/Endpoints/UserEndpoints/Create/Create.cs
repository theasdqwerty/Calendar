using Calendar.Application.Interfaces;
using FastEndpoints;

namespace Calendar.Web.Endpoints.UserEndpoints.Create;

/// <summary>
/// Create user endpoint.
/// </summary>
public class Create : Endpoint<CreateUserRequest, CreateUserResponse, UserMapper>
{
    private readonly IApplicationDbContext _dbContext;

    public Create(IApplicationDbContext dbContext) 
        => _dbContext = dbContext;

    public override void Configure()
    {
        Post("/users");
        Options(option => option.WithTags("Users"));
        Validator<CreateUserRequestValidator>();
        Summary(s => {
            s.Summary = "Create user.";
            s.Description = "Creating a user in a project.";
            s.ExampleRequest = new CreateUserRequest()
            {
                FirstName = "Ivan", 
                SecondName = "Ivanovich", 
                LastName = "Ivanov", 
                RegionCodeId = 52,
                Email = "youremail@email.com",
                PhoneNumber = "89008007060"
            };
            s.ResponseExamples[200] = 
                new CreateUserResponse()
                {
                    Id = 1, 
                    FirstName = "Ivan", 
                    SecondName = "Ivanovich", 
                    LastName = "Ivanov", 
                    RegionCodeId = 52,
                    Email = "youremail@email.com",
                    PhoneNumber = "89008007060"
                };
            s.Responses[200] = "User creation completed successfully.";
            s.Responses[403] = "User creation access denied.";
        });
        AllowAnonymous();
    }

    public override async Task HandleAsync(CreateUserRequest req, CancellationToken ct)
    {
        var entityEntry = await _dbContext.User.AddAsync(Map.ToEntity(req), ct);
        
        await _dbContext.SaveChangesAsync(ct);
        
        var response = Map.FromEntity(entityEntry.Entity);
        await SendAsync(response, cancellation: ct);
    }
}