using FastEndpoints;
using FluentValidation;

namespace Calendar.Web.Endpoints.UserEndpoints.Create;

public class CreateUserRequestValidator : Validator<CreateUserRequest>
{
    public CreateUserRequestValidator()
    {
        RuleFor(r => r.Email)
            .NotEmpty()
            .WithMessage("Email address is required");

        RuleFor(r => r.FirstName)
            .NotEmpty()
            .WithMessage("First name is required");

        RuleFor(r => r.LastName)
            .NotEmpty()
            .WithMessage("Last name is required");
    }
}