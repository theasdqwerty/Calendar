using Calendar.Domain.Entities;
using FastEndpoints;

namespace Calendar.Web.Endpoints.UserEndpoints.Create;

public class UserMapper : Mapper<CreateUserRequest, CreateUserResponse, User>
{
    public override CreateUserResponse FromEntity(User e)
    {
        return new CreateUserResponse()
        {
            Id = e.Id,
            FirstName = e.FirstName,
            SecondName = e.SecondName,
            LastName = e.LastName,
            RegionCodeId = e.RegionCodeId,
            Email = e.Email,
            PhoneNumber = e.PhoneNumber
        };
    }

    public override User ToEntity(CreateUserRequest r)
    {
        return new User()
        {
            FirstName = r.FirstName,
            SecondName = r.SecondName,
            LastName = r.LastName,
            RegionCodeId = r.RegionCodeId,
            Email = r.Email,
            PhoneNumber = r.PhoneNumber
        };
    }
}