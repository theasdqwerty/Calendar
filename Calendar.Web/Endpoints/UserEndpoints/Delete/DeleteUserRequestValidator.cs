using FastEndpoints;
using FluentValidation;

namespace Calendar.Web.Endpoints.UserEndpoints.Delete;

public class DeleteUserRequestValidator : Validator<DeleteUserRequest>
{
    public DeleteUserRequestValidator()
    {
        RuleFor(r => r.UserId)
            .GreaterThan(0)
            .WithMessage("User unique id cannot be 0");
    }
}