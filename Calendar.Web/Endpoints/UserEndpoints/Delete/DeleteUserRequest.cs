namespace Calendar.Web.Endpoints.UserEndpoints.Delete;

public class DeleteUserRequest
{
    public int UserId { get; set; }
}