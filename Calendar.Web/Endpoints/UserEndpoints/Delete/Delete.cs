using Calendar.Application.Enums;
using Calendar.Application.Interfaces;
using FastEndpoints;

namespace Calendar.Web.Endpoints.UserEndpoints.Delete;

public class Delete : Endpoint<DeleteUserRequest>
{
    private readonly IDeleteUserService _deleteUserService;

    public Delete(IDeleteUserService deleteUserService)
        => _deleteUserService = deleteUserService;
    
    public override void Configure()
    {
        Delete("/users/{userId:int}");
        Options(option => option.WithTags("Users"));
        Validator<DeleteUserRequestValidator>();
        Summary(s => {
            s.Summary = "Remove user.";
            s.Description = "Remove user from project";
            s.ExampleRequest = new DeleteUserRequest()
            {
                UserId = 100
            };
            s.Responses[200] = "User deleted successfully";
            s.Responses[403] = "User deleted access denied.";
        });
        AllowAnonymous();
    }

    public override async Task HandleAsync(DeleteUserRequest req, CancellationToken ct)
    {
        var result = await _deleteUserService.DeleteUser(req.UserId, ct);
        if (result == Result.NotFound)
        {
            await SendNotFoundAsync(ct);
            return;
        }

        await SendNoContentAsync(ct);
    }
}