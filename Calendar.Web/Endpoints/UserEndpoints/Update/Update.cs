using Calendar.Application.Interfaces;
using Calendar.Web.Endpoints.UserEndpoints.Create;
using FastEndpoints;

namespace Calendar.Web.Endpoints.UserEndpoints.Update;

/// <summary>
/// Update user endpoint.
/// </summary>
public class Update : Endpoint<UpdateUserRequest, UpdateUserResponse>
{
    private readonly IApplicationDbContext _dbContext;

    public Update(IApplicationDbContext dbContext) 
        => _dbContext = dbContext;

    public override void Configure()
    {
        Put("/users");
        Options(option => option.WithTags("Users"));
        Summary(s => {
            s.Summary = "Update user.";
            s.Description = "Update a user in a project.";
            s.ExampleRequest = new CreateUserRequest()
            {
                FirstName = "Ivan", 
                SecondName = "Ivanovich", 
                LastName = "Ivanov", 
                RegionCodeId = 52,
                Email = "youremail@email.com",
                PhoneNumber = "89008007060"
            };
            s.ResponseExamples[200] = 
                new CreateUserResponse()
                {
                    Id = 1, 
                    FirstName = "Ivan", 
                    SecondName = "Ivanovich", 
                    LastName = "Ivanov", 
                    RegionCodeId = 52,
                    Email = "youremail@email.com",
                    PhoneNumber = "89008007060"
                };
            s.Responses[200] = "User update completed successfully.";
            s.Responses[403] = "User update access denied.";
        });
        AllowAnonymous();
    }

    public override async Task HandleAsync(UpdateUserRequest req, CancellationToken ct)
    {
        // TODO доделать подрубить бд.
        var response = new UpdateUserResponse()
        {
        };

        await SendAsync(response, cancellation: ct);
    }
}