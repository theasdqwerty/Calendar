using Calendar.Application;
using Calendar.Application.Interfaces;
using Calendar.Domain;
using Calendar.Infrastructure;
using Calendar.Infrastructure.Data;
using FastEndpoints;
using FastEndpoints.Swagger;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var configuration = new ConfigurationBuilder()
    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
    .AddJsonFile("appsettings.json")
    .Build();

builder.Services.AddEntityFrameworkSqlite().AddDbContext<TestingDbContext>();
using (var db = new TestingDbContext())
{
    db.Database.EnsureCreated();
    db.Database.Migrate();
}

// var connectionString = configuration[$"ConnectionStrings:Sqlite"];
// builder.Services.AddSqlite<ApplicationDbContext>(connectionString);
// var connectionString = configuration[$"ConnectionStrings:PostgreSQL"];
// builder.Services.AddDbContext<ApplicationDbContext>(cfg => cfg.UseNpgsql(connectionString));
builder.Services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<ApplicationDbContext>());

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddFastEndpoints();
builder.Services.SwaggerDocument(options =>
{
    options.AutoTagPathSegmentIndex = 0;
    options.DocumentSettings = settings =>
    {
        settings.DocumentName = "Testing version";
        settings.Title = "Garden calendar open api";
        settings.Version = "1.0.0";
    };
});

// Dependency injection
builder.Services
    .AddDomain()
    .AddApplication()
    .AddInfrastructure();

var app = builder.Build();

// // Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
//     app.UseSwagger(options => { });
//     app.UseSwaggerUI();
// }

app.UseRouting();
app.UseFastEndpoints(cfg => cfg.Endpoints.ShortNames = true);
app.UseSwaggerGen();
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();